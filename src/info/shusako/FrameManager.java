package info.shusako;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

/**
 * Created by Shusako on 2/18/2016.
 * For project Misc, 2016
 */
public class FrameManager {

    private JFrame frame;

    private static FrameManager instance = new FrameManager();

    public static FrameManager getInstance() {
        return instance;
    }

    private FrameManager() {
        initFrame();
    }

    private void initFrame() {
        frame = new JFrame();
        frame.setTitle("PathFinding");
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.setResizable(false);

        final JComponent component = new JComponent() {
            @Override
            public void paint(Graphics graphics) {
                World.getInstance().draw(graphics);

                try {
                    Thread.sleep((1000L) / (10L));
                    this.repaint();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };
        component.setPreferredSize(new Dimension(World.getTotalDimensions(), World.getTotalDimensions()));

        MouseAdapter mouseAdapter = new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                actionOn(e.getX(), e.getY());
            }

            @Override
            public void mouseDragged(MouseEvent e) {
                actionOn(e.getX(), e.getY());
            }

            @Override
            public void mouseMoved(MouseEvent e) {
                MousePosition.setMousePosition(e.getX(), e.getY());
            }

            @Override
            public void mouseReleased(MouseEvent e) {
                lastChangeX = lastChangeY = -1;
            }

            private int lastChangeX, lastChangeY;

            public void actionOn(int localX, int localY) {
                localX /= World.getBLOCK_SIZE();
                localY /= World.getBLOCK_SIZE();

                if(localX == lastChangeX && localY == lastChangeY) return;

                if(World.getInstance().getTile(localX, localY) == Tile.EMPTY) {
                    World.getInstance().setTile(localX, localY, Tile.WALL);
                } else {
                    World.getInstance().setTile(localX, localY, Tile.EMPTY);
                }

                lastChangeX = localX;
                lastChangeY = localY;
            }
        };

        component.addMouseListener(mouseAdapter);
        component.addMouseMotionListener(mouseAdapter);

        KeyAdapter keyAdapter = new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                switch (e.getKeyChar()) {
                    case 's':
                        Tile.START.setPosition(MousePosition.getLocalX(), MousePosition.getLocalY());
                        break;
                    case 'e':
                        Tile.STOP.setPosition(MousePosition.getLocalX(), MousePosition.getLocalY());
                        break;
                    case 'f':
                        PathFinder.runSearchThread();
                        break;
                    case 'c':
                        World.refreshAll();
                        break;
                    case 'g':
                        PathFinder.clearProofOfSearch();
                        break;
                }
                super.keyPressed(e);
            }
        };

        frame.addKeyListener(keyAdapter);

        frame.add(component);
        frame.pack();

        frame.setVisible(true);
        frame.setLocationRelativeTo(null);
    }
}
