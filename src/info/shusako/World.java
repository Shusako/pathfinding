package info.shusako;

import java.awt.*;
import java.awt.geom.AffineTransform;
import java.text.DecimalFormat;

/**
 * Created by Shusako on 2/18/2016.
 * For project Misc, 2016
 */
public class World {

    private final Node[][] world;

    private static World instance = new World();

    public static World getInstance() {
        return instance;
    }

    public static void refreshAll() {
        instance = new World();
    }

    private World() {
        world = new Node[WORLD_DIMENSION][WORLD_DIMENSION];
        populateWorld();
    }

    private void populateWorld() {
        for(int i = 0; i < WORLD_DIMENSION; i ++) {
            for(int j = 0; j < WORLD_DIMENSION; j ++) {
                Node node = new Node(i, j);
                node.setTile((i == 0 || i == WORLD_DIMENSION - 1 || j == 0 || j == WORLD_DIMENSION - 1) ? Tile.BORDER : Tile.EMPTY);
                world[i][j] = node;
            }
        }
    }

    public void setTile(int i, int j, Tile tile) {
        if(i >= 0 && j >= 0 && i < WORLD_DIMENSION && j < WORLD_DIMENSION) {
            world[i][j].setTile(tile);
        }
    }

    public Tile getTile(int i, int j) {
        if(i >= 0 && j >= 0 && i < WORLD_DIMENSION && j < WORLD_DIMENSION) {
            return world[i][j].getTile();
        }
        return Tile.EMPTY;
    }

    private static final int WORLD_DIMENSION = 15 + 2;
    private static final int BLOCK_SIZE = 63;

    public Node[][] getNodeMap() {
        return world;
    }

    public static int getBLOCK_SIZE() {
        return BLOCK_SIZE;
    }

    public static int getWORLD_DIMENSION() {
        return WORLD_DIMENSION;
    }

    public static int getTotalDimensions() {
        return BLOCK_SIZE * WORLD_DIMENSION;
    }

    public void draw(Graphics graphics) {
        DecimalFormat decimalFormat = new DecimalFormat("#.#");

        for(int i = 0; i < WORLD_DIMENSION; i ++) {
            for(int j = 0; j < WORLD_DIMENSION; j ++) {
                graphics.setColor(world[i][j].getTile().getColor());
                graphics.fillRect(i * BLOCK_SIZE, j * BLOCK_SIZE, BLOCK_SIZE, BLOCK_SIZE);

                graphics.setColor(Color.BLACK);
                graphics.drawRect(i * BLOCK_SIZE, j * BLOCK_SIZE, BLOCK_SIZE, BLOCK_SIZE);

                if(BLOCK_SIZE >= 50) {
                    int xOffset = 2;
                    int yOffset = 2;

                    Node node = world[i][j];

                    if(node.getParent() != null) {
                        graphics.drawString("H: " + decimalFormat.format(node.getHeuristic()), i * BLOCK_SIZE + xOffset, j * BLOCK_SIZE + (yOffset += 12));
                        graphics.drawString("G: " + decimalFormat.format(node.getgCost()), i * BLOCK_SIZE + xOffset, j * BLOCK_SIZE + (yOffset += 12));
                        graphics.drawString("F: " + decimalFormat.format(node.getfValue()), i * BLOCK_SIZE + xOffset, j * BLOCK_SIZE + (yOffset += 12));
                        int endX = getClosestTripoint(i * BLOCK_SIZE + (BLOCK_SIZE / 2), node.getParent().posX() * BLOCK_SIZE + (BLOCK_SIZE / 2));
                        int endY = getClosestTripoint(j * BLOCK_SIZE + (BLOCK_SIZE / 2), node.getParent().posY() * BLOCK_SIZE + (BLOCK_SIZE / 2));
                        drawArrow(graphics, i * BLOCK_SIZE + (BLOCK_SIZE / 2), j * BLOCK_SIZE + (BLOCK_SIZE / 2), endX, endY);
                    }
                }
            }
        }
    }

    private int getClosestTripoint(int x, int x2) {
        return (int) (x * (2F / 3) + x2 * (1F / 3));
    }

    private final int ARR_SIZE = 4;
    // http://stackoverflow.com/questions/4112701/drawing-a-line-with-arrow-in-java
    void drawArrow(Graphics g1, int x1, int y1, int x2, int y2) {
        Graphics2D g = (Graphics2D) g1.create();

        double dx = x2 - x1, dy = y2 - y1;
        double angle = Math.atan2(dy, dx);
        int len = (int) Math.sqrt(dx*dx + dy*dy);
        AffineTransform at = AffineTransform.getTranslateInstance(x1, y1);
        at.concatenate(AffineTransform.getRotateInstance(angle));
        g.transform(at);

        // Draw horizontal arrow starting in (0, 0)
        g.drawLine(0, 0, len, 0);
        g.fillPolygon(new int[] {len, len-ARR_SIZE, len-ARR_SIZE, len},
                new int[] {0, -ARR_SIZE, ARR_SIZE, 0}, 4);
    }
}
