package info.shusako;

/**
 * Created by Shusako on 2/18/2016.
 * For project Misc, 2016
 */
public class MousePosition {

    private static int mouseX, mouseY;
    private static int localX, localY;

    public static void setMousePosition(int mouseX, int mouseY) {
        MousePosition.mouseX = mouseX;
        MousePosition.mouseY = mouseY;

        MousePosition.localX = mouseX / World.getBLOCK_SIZE();
        MousePosition.localY = mouseY / World.getBLOCK_SIZE();
    }

    public static int getMouseX() {
        return mouseX;
    }

    public static int getMouseY() {
        return mouseY;
    }

    public static int getLocalX() {
        return localX;
    }

    public static int getLocalY() {
        return localY;
    }
}
