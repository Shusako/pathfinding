package info.shusako;

/**
 * Created by Shusako on 2/18/2016.
 * For project Misc, 2016
 */
public class Node {

    private int posX, posY;

    private double heuristic;
    private double fValue;
    private double gCost;

    private Node parent;

    private Tile tile;

    public Node(int posX, int posY) {
        this.posX = posX;
        this.posY = posY;
    }

    public int posX() {
        return posX;
    }

    public int posY() {
        return posY;
    }

    public double getHeuristic() {
        return heuristic;
    }

    public void setHeuristic(double heuristic) {
        this.heuristic = heuristic;
    }

    public double getfValue() {
        return fValue;
    }

    public void setfValue(double fValue) {
        this.fValue = fValue;
    }

    public double getgCost() {
        return gCost;
    }

    public void setgCost(double gCost) {
        this.gCost = gCost;
    }

    public Tile getTile() {
        return tile;
    }

    public void setTile(Tile tile) {
        if(this.tile == null) {
            this.tile = tile;
        } else
        if(!this.tile.isLocked()) {
            this.tile = tile;
        }
    }

    public Node getParent() {
        return parent;
    }

    public void setParent(Node parent) {
        this.parent = parent;
    }
}
