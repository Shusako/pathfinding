package info.shusako;

import java.awt.*;

/**
 * Created by Shusako on 2/18/2016.
 * For project Misc, 2016
 */
public enum Tile {

    EMPTY(0, Color.WHITE, false, 1), SEARCHED(5, Color.LIGHT_GRAY, false, -1), PATH(6, Color.CYAN, false, -1), WALL(1, Color.BLACK, false, -1), BORDER(2, Color.BLACK, true, -1), START(3, Color.GREEN, true, 1), STOP(4, Color.RED, true, 1);

    private int posX = Integer.MIN_VALUE, posY = Integer.MIN_VALUE;

    private int id;
    private Color color;
    private boolean locked;
    private int moveCost;

    Tile(int id, Color color, boolean locked, int moveCost) {
        this.id = id;
        this.color = color;
        this.locked = locked;
        this.moveCost = moveCost;
    }

    public int getMoveCost() {
        return moveCost;
    }

    public int getId() {
        return id;
    }

    public Color getColor() {
        return color;
    }

    public boolean isLocked() {
        return locked;
    }

    public void setPosition(int posX, int posY) {
        this.locked = false;
        World.getInstance().setTile(this.posX, this.posY, EMPTY);
        this.locked = true;

        if(World.getInstance().getTile(posX, posY).isLocked()) {
            this.posX = Integer.MIN_VALUE;
            this.posY = Integer.MIN_VALUE;
        } else {
            World.getInstance().setTile(posX, posY, this);
            this.posX = posX;
            this.posY = posY;
        }
    }

    public int posX() {
        return posX;
    }

    public int posY() {
        return posY;
    }
}
