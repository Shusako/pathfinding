package info.shusako;

/**
 * Created by Shusako on 2/18/2016.
 * For project Misc, 2016
 */
public class PathFinder {

    public static void clearProofOfSearch() {
        Node[][] nodeMap = World.getInstance().getNodeMap();

        running = false;

        for(int i = 0; i < World.getWORLD_DIMENSION(); i ++) {
            for(int j = 0; j < World.getWORLD_DIMENSION(); j ++) {
                Node testNode = nodeMap[i][j];
                if(testNode.getTile() == Tile.SEARCHED || testNode.getTile() == Tile.PATH) {
                    testNode.setTile(Tile.EMPTY);
                }

                testNode.setfValue(0);
                testNode.setgCost(0);
                testNode.setHeuristic(0);
                testNode.setParent(null);
            }
        }

    }

    private static boolean running = false;

    public static void search() {
        World world = World.getInstance();
        Node[][] nodeMap = world.getNodeMap();

        clearProofOfSearch();
        running = true;

        calculateHeuristics(nodeMap);

        boolean[][] open = new boolean[World.getWORLD_DIMENSION()][World.getWORLD_DIMENSION()];
        boolean[][] closed = new boolean[World.getWORLD_DIMENSION()][World.getWORLD_DIMENSION()];

        int currentX = Tile.START.posX();
        int currentY = Tile.START.posY();
        int stopX = Tile.STOP.posX();
        int stopY = Tile.STOP.posY();

        int counter = 0;

        out:
        while(counter ++ < 5000) {
            try {
                Thread.sleep(25);
                if(!running) {
                    break;
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            System.out.println(counter);
            Node centerNode = nodeMap[currentX][currentY];

            //check around it, calculate the movement cost for those nodes, and then the f cost for them
            for (int i = -1; i <= 1; i++) {
                for (int j = -1; j <= 1; j++) {
                    if (i == 0 && j == 0) continue;
//                    if (i != 0 && j != 0) continue;
                    if (i != 0 && j != 0) {
                        if(nodeMap[currentX + i][currentY].getTile().getMoveCost() < 0) continue;
                        if(nodeMap[currentX][currentY + j].getTile().getMoveCost() < 0) continue;
                    }
                    if (closed[currentX + i][currentY + j]) {
                        // do nothing
                        continue;
                    }
                    Node node = nodeMap[currentX + i][currentY + j];
                    if(node.getTile().getMoveCost() < 0) continue;

                    double moveCost = node.getTile().getMoveCost();
                    if(i != 0 && j != 0) moveCost *= Math.sqrt(2);

                    if (open[currentX + i][currentY + j]) {
                        // if its a better path from the centerNode rather than its old path, change it
                        if (centerNode.getgCost() + moveCost < node.getgCost()) {
                            node.setgCost(centerNode.getgCost() + moveCost);
                            node.setfValue(node.getHeuristic() + node.getgCost());
                            node.setParent(centerNode);
                        }
                        // https://www.youtube.com/watch?v=KNXfSOx4eEE
                    } else {
                        // set parenting
                        node.setgCost(centerNode.getgCost() + moveCost);
                        node.setfValue(node.getHeuristic() + node.getgCost());
                        node.setParent(centerNode);

                        open[currentX + i][currentY + j] = true;
                    }

                    if(node.getTile() == Tile.STOP) {
//                        node.setParent(centerNode);
                        break out;
                    }
                }
            }

            open[currentX][currentY] = false;
            closed[currentX][currentY] = true;

            if(centerNode.getTile() != Tile.START) {
                centerNode.setTile(Tile.SEARCHED);
            }

            // find next center node (by least f value)
            Node currentChoice = null;
            double fValue = Double.MAX_VALUE;
            for(int i = 0; i < World.getWORLD_DIMENSION() - 1; i ++) {
                for(int j = 0; j < World.getWORLD_DIMENSION() - 1; j ++) {
                    if(open[i][j]) {
                        Node trialNode = nodeMap[i][j];
                        if(trialNode.getTile().getMoveCost() < 0) {
                            continue;
                        }
                        if(trialNode.getfValue() < fValue) {
                            currentChoice = trialNode;
                            fValue = trialNode.getfValue();
                        }
                    }
                }
            }

            if(currentChoice == null) break;
            currentX = currentChoice.posX();
            currentY = currentChoice.posY();
        }


        running = false;

        if(stopX == Integer.MIN_VALUE && stopY == Integer.MIN_VALUE) return;

        Node finalNode = nodeMap[stopX][stopY];
        while(finalNode.getParent() != null) {
            finalNode = finalNode.getParent();
            if(finalNode.getTile() == Tile.START) break;
            finalNode.setTile(Tile.PATH);
        }
    }

    public static void calculateHeuristics(Node[][] nodeMap) {
        int centerX = Tile.STOP.posX();
        int centerY = Tile.STOP.posY();

        if(centerX == Integer.MIN_VALUE && centerY == Integer.MIN_VALUE) {
            for(int i = 0; i < World.getWORLD_DIMENSION(); i ++) {
                for(int j = 0; j < World.getWORLD_DIMENSION(); j ++) {
                    nodeMap[i][j].setHeuristic(0);
                }
            }

            return;
        }

        for(int i = 0; i < World.getWORLD_DIMENSION() - 1; i ++) {
            for(int j = 0; j < World.getWORLD_DIMENSION() - 1; j ++) {
                //manhatten distance
                double distance = Math.sqrt(Math.pow(centerX - i, 2) + Math.pow(centerY - j, 2));
                nodeMap[i][j].setHeuristic(distance);
            }
        }
    }

    public static void runSearchThread() {
        if(running) return;
        new Thread(new Runnable() {
            @Override
            public void run() {
                PathFinder.search();
            }
        }).start();
    }
}
